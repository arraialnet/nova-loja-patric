<?php

class Cart extends Model {

    public function getList() {
        $products = new Products();

        $array = array();

        $cart = array();

        if(isset($_SESSION['cart'])) {
            $cart = $_SESSION['cart'];
        }
       
        foreach($cart as $id => $qt) {

            $info = $products->getInfo($id);
            $total = $info['price'] + 10;
            $array[] = array(
               'name' => $info['name'],
                'id' => $id,   
                'qt' => $qt,
                'price' => $info['price'],
                'valor_frete' => 10,
                'subtotal' => $info['price'],
                'total' => $total,
                'image' => $info['image'],
                
            );
        }

        return $array;
    }

    
    public function salvarInfo($dados){
         
        $sql = "INSERT INTO compras SET ";
        $data = array();
        foreach($dados['i'] as $key => $value){
           $data[] = $key." ='".$value."'";
        }
        $sql = $sql.implode(", ", $data);
        
        //echo $sql;

        $sql = $this->db->query($sql);
        unset($_SESSION['cart']);

      }

      

    public function getSubTotal() {

        $list = $this->getList();

        $subtotal = 0;

        foreach ($list as $item) {

            $subtotal += (floatval($item['price']) * intval($item['qt'])  );

        }

        return  $subtotal;

    }


}