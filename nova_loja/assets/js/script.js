//08-04-19 - alterado urls com http://localhost/nova_loja/... para BASE_URL+'login'
$(function(){
    $(".conf button").on("click",function(){

      $.ajax({
      type:'POST',
      url:BASE_URL+'login/verificaLogin',
      data:{},
      success:function(msg) {
        if(msg == '1'){
             location.href = BASE_URL+"cart/salvarInfo/";
        }else{
            var r = "voce precisa fazer login para confirmar a conta !";
            location.href = BASE_URL+"login";
        }
      }
        
    });
    });

    $('#form').on('submit', function(e) {
      e.preventDefault();
  
      var email = $('input[name=email]').val();
      var senha = $('input[name=password]').val();

      $.ajax({
        type:'POST',
        url:BASE_URL+'login/ajax/',
        data:{email:email, senha:senha},
        success:function(msg) {
          if(msg == '1'){
                location.href = BASE_URL;
            }else{
                $(".msg").html(msg);
            }
          }
                      
      });
  
    });
  
  
  if(typeof maxslider != 'undefined') {


    $( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: maxslider,
      values: [ $('#slider0').val(), $('#slider1').val()],
      slide: function( event, ui ) {
        $( "#amount" ).val( "R$" + ui.values[ 0 ] + " - R$" + ui.values[ 1 ] );
      },
      change: function( event, ui ) {
        $('#slider'+ui.handleIndex).val(ui.value);
        
        $('.filterarea form').submit();
      }

    });  

  }
 

  $( "#amount" ).val( "R$ " + $( "#slider-range" ).slider( "values", 0 ) +
      " - R$ " + $( "#slider-range" ).slider( "values", 1 ) );

    
  $('.filterarea').find('input').on('change', function(){
    $('.filterarea form').submit();

  });

    

    $('.addtocartform button').on('click', function(e) {
      e.preventDefault();

      var qt = parseInt($('.addtocart_qt').val());

      var action = $(this).attr('data-action');

      if(action == 'decrease') {

        if(qt-1 >= 1) {
          qt = qt - 1;
        }

      } else if(action="increase") {
        qt = qt + 1;
      }

      $('.addtocart_qt').val(qt);

      $('input[name=qt_product]').val(qt);

      

      
    });

    $('.photo_item').on('click', function() {
      var url= $(this).find('img').attr('src');
      $('.mainphoto').find('img').attr('src', url);

    });
    

});
    
