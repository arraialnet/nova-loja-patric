<?php

class Store extends Model {

    public function getTemplateData() {
        $dados = array();

        $categories = new Categories();

        $products = new Products();

        $cart = new Cart();

        $dados['categories'] = $categories->getList();

        $dados['widgets_featured'] = $products->getList(0, 3, array('featured'=>'1'), true);

        $dados['widgets_sale'] = $products->getList(0, 3, array('sale'=>'1'), true);

        $dados['widgets_toprated'] = $products->getList(0, 3, array('toprated'=>'1'));

        if(isset($_SESSION['cart'])) {

            $qt = 0;

            foreach($_SESSION['cart'] as $qtd) {

                $qt += $qtd;

            }

            $dados['cart_qt'] = $qt;

        } else {
            $dados['cart_qt'] = 0;
        }

        $dados['cart_subtotal'] = $cart->getSubTotal();
        


		return $dados;
    }

}