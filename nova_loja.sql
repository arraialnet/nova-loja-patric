-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 07-Abr-2019 às 03:47
-- Versão do servidor: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `nova_loja`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `brands`
--

CREATE TABLE IF NOT EXISTS `brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Extraindo dados da tabela `brands`
--

INSERT INTO `brands` (`id`, `name`) VALUES
(1, 'gigabyte'),
(2, 'asus'),
(3, 'geforce'),
(4, 'nvidia'),
(5, 'INTEL'),
(6, 'CORSHAIR');

-- --------------------------------------------------------

--
-- Estrutura da tabela `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sub` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Extraindo dados da tabela `categories`
--

INSERT INTO `categories` (`id`, `sub`, `name`) VALUES
(1, NULL, 'placas'),
(2, NULL, 'som'),
(3, 2, 'headphone'),
(5, 3, 'com fio'),
(6, NULL, 'placa mãe'),
(7, NULL, 'memória RAM'),
(8, NULL, 'PC Gamer'),
(9, NULL, 'Processador');

-- --------------------------------------------------------

--
-- Estrutura da tabela `clientes`
--

CREATE TABLE IF NOT EXISTS `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `senha` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `clientes`
--

INSERT INTO `clientes` (`id`, `Nome`, `email`, `senha`) VALUES
(1, 'pck', 'pck@pck.com', '123');

-- --------------------------------------------------------

--
-- Estrutura da tabela `compras`
--

CREATE TABLE IF NOT EXISTS `compras` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_compra` varchar(32) NOT NULL,
  `id_product` int(11) NOT NULL,
  `id_user` int(30) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `price` int(100) DEFAULT NULL,
  `qt` int(100) DEFAULT NULL,
  `subtotal` int(100) DEFAULT NULL,
  `cep` int(100) DEFAULT NULL,
  `valor_frete` int(100) DEFAULT NULL,
  `total` int(100) DEFAULT NULL,
  `image` varchar(250) NOT NULL,
  `data` datetime(6) NOT NULL,
  `status` int(5) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=85 ;

--
-- Extraindo dados da tabela `compras`
--

INSERT INTO `compras` (`id`, `id_compra`, `id_product`, `id_user`, `name`, `price`, `qt`, `subtotal`, `cep`, `valor_frete`, `total`, `image`, `data`, `status`) VALUES
(75, '131', 0, 5, 'placa punk', 30, 1, 30, NULL, 10, 40, '', '0000-00-00 00:00:00.000000', 3),
(76, '131', 0, 5, 'placa show de bola', 700, 1, 700, NULL, 10, 710, '', '0000-00-00 00:00:00.000000', 3),
(77, '131', 0, 5, 'be', 500, 2, 1000, NULL, 10, 1010, '', '0000-00-00 00:00:00.000000', 3),
(78, '131', 0, 5, '1', 50, 4, 200, NULL, 10, 210, '', '0000-00-00 00:00:00.000000', 3),
(81, '1311553220714', 0, 5, 'placa punk', 30, 1, 30, NULL, 10, 40, '', '0000-00-00 00:00:00.000000', 3),
(82, '1311553220714', 0, 5, 'be', 500, 1, 500, NULL, 10, 510, '', '0000-00-00 00:00:00.000000', 1),
(83, '1311553220769', 0, 5, 'placa do futuro', 20, 2, 20, NULL, 10, 30, '', '0000-00-00 00:00:00.000000', 1),
(84, '1311553220769', 0, 5, '1', 50, 2, 100, NULL, 10, 110, '', '0000-00-00 00:00:00.000000', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `coupons`
--

CREATE TABLE IF NOT EXISTS `coupons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `coupon_type` int(11) NOT NULL,
  `coupon_value` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `locais`
--

CREATE TABLE IF NOT EXISTS `locais` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `bairro_name` varchar(50) NOT NULL,
  `frete_valor` int(50) NOT NULL,
  `tempo_espera` int(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `locais`
--

INSERT INTO `locais` (`id`, `bairro_name`, `frete_valor`, `tempo_espera`) VALUES
(1, 'aldeota', 10, 10);

-- --------------------------------------------------------

--
-- Estrutura da tabela `options`
--

CREATE TABLE IF NOT EXISTS `options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `options`
--

INSERT INTO `options` (`id`, `name`) VALUES
(1, 'Velocidade'),
(2, 'Voltagem');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `body` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_category` int(11) NOT NULL,
  `id_brand` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text,
  `stock` int(11) NOT NULL,
  `price` float NOT NULL,
  `price_from` float NOT NULL,
  `rating` float NOT NULL,
  `featured` tinyint(4) NOT NULL,
  `sale` tinyint(4) NOT NULL,
  `bestseller` tinyint(4) NOT NULL,
  `new_product` tinyint(4) NOT NULL,
  `options` varchar(200) DEFAULT NULL,
  `weight` float NOT NULL,
  `width` float NOT NULL,
  `height` float NOT NULL,
  `length` float NOT NULL,
  `diameter` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

--
-- Extraindo dados da tabela `products`
--

INSERT INTO `products` (`id`, `id_category`, `id_brand`, `name`, `description`, `stock`, `price`, `price_from`, `rating`, `featured`, `sale`, `bestseller`, `new_product`, `options`, `weight`, `width`, `height`, `length`, `diameter`) VALUES
(1, 1, 1, 'placa de vï¿½deo ', '<p>alguma desc</p>', 10, 100, 200, 0, 0, 1, 1, 0, '', 0.9, 1, 2, 20, 36),
(2, 2, 2, 'placa show de bola', 'some desc', 20, 700, 0, 2, 0, 0, 1, 0, '1,2', 0.8, 2, 1, 143, 25),
(3, 1, 2, 'placa punk', 'isso', 10, 30, 50, 5, 1, 0, 0, 1, '1,2', 0.7, 9, 2, 20, 48),
(4, 1, 1, 'placa do futuro', '<p>isto</p>', 50, 20, 30, 2, 1, 0, 0, 0, '', 0.3, 2, 2, 324, 6),
(15, 1, 6, 'Memória 4gb', 'foi', 0, 500, 700, 2, 1, 1, 0, 0, '1,2', 0.7, 2, 2, 20, 75),
(16, 1, 6, 'Memória 16 gb', 'certo', 10, 500, 700, 3, 1, 0, 0, 0, '1', 0.8, 2, 2, 46, 36),
(17, 1, 1, 'be', 'c', 40, 500, 600, 3, 1, 1, 0, 0, '1', 0.4, 2, 2, 46, 26),
(19, 1, 1, '1', '1', 0, 400, 800, 0, 0, 0, 0, 0, '1', 0.1, 2, 2, 35, 68),
(20, 1, 1, '1', '1', 1, 400, 800, 0, 0, 0, 0, 0, '1', 0.5, 2, 2, 47, 23),
(21, 1, 1, '1', 'oijsofiasipofasda', 0, 50, 800, 3, 0, 1, 0, 0, '2', 0.7, 2, 2, 25, 23),
(31, 1, 1, 'TESTSE', '<p>1</p>', 1, 1, 1, 0, 0, 0, 0, 0, '1,2', 1, 1, 1, 1, 1),
(32, 1, 1, 'TESTSE', '<p>1</p>', 1, 1, 1, 0, 0, 0, 0, 0, '1,2', 1, 1, 1, 1, 1),
(33, 1, 1, 'TESTSE', '<p>1</p>', 1, 1, 1, 0, 0, 0, 0, 0, '1,2', 1, 1, 1, 1, 1),
(34, 1, 1, 'sera', '<p>2</p>', 2, 1, 2, 0, 0, 0, 0, 0, '1,2', 2, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `products_images`
--

CREATE TABLE IF NOT EXISTS `products_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_product` int(11) NOT NULL,
  `url` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

--
-- Extraindo dados da tabela `products_images`
--

INSERT INTO `products_images` (`id`, `id_product`, `url`) VALUES
(1, 1, 'a.jpg'),
(2, 2, '2.jpg'),
(3, 3, '3.jpg'),
(4, 4, '4.jpg'),
(5, 15, '5.jpg'),
(6, 16, '6.jpg'),
(7, 17, '7.jpg'),
(8, 18, '8.jpg'),
(9, 19, '9.jpg'),
(10, 20, '10.jpg'),
(11, 21, '11.jpg'),
(13, 2, '3.jpg'),
(14, 2, '4.jpg'),
(15, 2, '5.jpg'),
(16, 22, '1554335870.jpg'),
(17, 23, '1554335961.jpg'),
(18, 24, '1554335996.jpg'),
(19, 25, '1554336173.jpg'),
(20, 26, '1554336174.jpg'),
(21, 27, '1554336419.jpg'),
(22, 28, '1554338711.jpg'),
(23, 29, '1554338866.jpg'),
(24, 30, '1554339109.jpg'),
(25, 31, '1554341024.jpg'),
(26, 32, '1554341197.jpg'),
(27, 33, '1554341198.jpg'),
(28, 34, '1554341226.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `products_options`
--

CREATE TABLE IF NOT EXISTS `products_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_option` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `p_value` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=40 ;

--
-- Extraindo dados da tabela `products_options`
--

INSERT INTO `products_options` (`id`, `id_option`, `id_product`, `p_value`) VALUES
(1, 1, 1, '2 gbs'),
(2, 2, 1, '200 wts'),
(3, 1, 2, '1 gbs'),
(4, 2, 2, '300 wts'),
(6, 1, 3, '1 gbs'),
(7, 2, 3, '100 rms'),
(8, 1, 3, '2 gbs'),
(9, 2, 3, '100 rms'),
(10, 1, 4, '2 gbs'),
(11, 2, 4, '100 rms'),
(12, 1, 5, '4 gbs'),
(13, 2, 5, '100 rms'),
(14, 1, 6, '4 gbs'),
(15, 2, 6, '100 rms'),
(16, 1, 7, '8 gbs'),
(17, 2, 7, '200 rms'),
(18, 1, 22, '5'),
(19, 2, 22, '5'),
(20, 1, 23, '5'),
(21, 2, 23, '5'),
(22, 1, 24, '2'),
(23, 2, 24, '2'),
(24, 1, 25, '2'),
(25, 2, 25, '2'),
(26, 1, 26, '2'),
(27, 2, 26, '2'),
(28, 1, 27, '2'),
(29, 2, 27, '2'),
(30, 1, 28, '1'),
(31, 2, 28, '1'),
(32, 1, 31, '1'),
(33, 2, 31, '1'),
(34, 1, 32, '1'),
(35, 2, 32, '1'),
(36, 1, 33, '1'),
(37, 2, 33, '1'),
(38, 1, 34, '1'),
(39, 2, 34, '1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `purchases`
--

CREATE TABLE IF NOT EXISTS `purchases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_coupon` int(11) NOT NULL,
  `total_amount` int(11) NOT NULL,
  `payment_type` int(11) DEFAULT NULL,
  `payment_status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `purchases_products`
--

CREATE TABLE IF NOT EXISTS `purchases_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_purchase` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `purchase_transactions`
--

CREATE TABLE IF NOT EXISTS `purchase_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_purchase` int(11) NOT NULL,
  `amount` float NOT NULL,
  `transaction_code` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `rates`
--

CREATE TABLE IF NOT EXISTS `rates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_product` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `date_rated` datetime NOT NULL,
  `points` int(11) NOT NULL,
  `comment` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `rates`
--

INSERT INTO `rates` (`id`, `id_product`, `id_user`, `date_rated`, `points`, `comment`) VALUES
(1, 2, 1, '1899-11-30 03:09:10', 2, 'Gostei placa mt boa');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `pass` varchar(50) NOT NULL,
  `username` varchar(100) NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `loginhash` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `email`, `pass`, `username`, `admin`, `loginhash`) VALUES
(1, '2@2comn', '123', 'pck', 0, NULL),
(4, '2@2com', '$2y$10$ec/QXAJ5ec1FO/E1C1e8/uTEJygsRtWBAJIh5gA8fi8', 'a', 0, NULL),
(5, 'a@a.com', '202cb962ac59075b964b07152d234b70', '0', 0, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
