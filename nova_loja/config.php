<?php
require 'environment.php';

global $config;
global $db;

$config = array();
if(ENVIRONMENT == 'development') {
	define("BASE_URL", "http://localhost/nova_loja/");
	$config['dbname'] = 'nova_loja';
	$config['host'] = 'localhost';
	$config['dbuser'] = 'root';
	$config['dbpass'] = '';
} else {
	define("BASE_URL", "http://loja.macarraoarretado.com.br/");
	$config['dbname'] = 'cemdr297_loja';
	$config['host'] = 'localhost';
	$config['dbuser'] = 'cemdr297_loja';
	$config['dbpass'] = 'a92314698';
}

$config['cep_origin'] = '60040400';


$db = new PDO("mysql:dbname=".$config['dbname'].";host=".$config['host'], $config['dbuser'], $config['dbpass']);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
?>