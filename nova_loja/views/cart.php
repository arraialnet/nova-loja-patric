<table border="0" width="100%">

<tr>
    <th width="100" > Imagem </th>
    <th> Nome </th>
    <th width="130"> Quantidade </th>
    <th width="120"> Preço </th>
    <th width="20">  </th>

</tr>

<?php 
$subtotal = 0;
?>

<?php foreach($list as $item): ?>

<?php 
$subtotal += (floatval($item['price']) * intval($item['qt']) );
?>

<tr>
    <td> <img src="<?php echo BASE_URL; ?>/media/products/<?php echo $item['image']; ?> " width ="80" </td>
    <td> <?php echo $item['name']; ?> </td>
    <td> <?php echo $item['qt']; ?> </td>
    <td> R$ <?php echo number_format($item['price'], 2, ',', '.'); ?> </td>
    <td> <a href="<?php echo BASE_URL; ?>cart/del/<?php echo $item['id']; ?>"> <img src="<?php echo BASE_URL; ?>assets/images/delete.png" width="20" /> </td>

</tr>

<?php endforeach; ?>

    <tr>
        <td colspan="3" align="right"> Sub-total dos Produtos: </td>

        <td><strong> R$ <?php echo number_format($subtotal, 2, ',', '.'); ?> </strong> </td>

    </tr>

    <tr>
        <td colspan="3" align="right"> Frete: </td>

        <td>

        <?php if(isset($shipping['price'])): ?>

            <strong> R$ <?php echo $shipping['price']; ?> </strong>

        <?php else: ?>        
            Qual o seu CEP?? </br>

            <form method="POST">

                    <input type="number" name="cep"  /><br/>

                    <input type="submit" value="Calcular"  />
                
            </form>  
        <?php endif; ?>     
        
         </td>

    </tr>

    <tr>

        <td colspan="3" align="right"> Total: </td>

        <td><strong> R$ <?php echo number_format($subtotal, 2, ',', '.'); ?> </strong> </td>

    </tr>

</table>




<br/><br/><br/>
<div class="conf"  > 
    <button>confirmar</button>



</div>