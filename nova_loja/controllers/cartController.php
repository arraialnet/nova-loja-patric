<?php
class cartController extends Controller {

    private $user;

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $store = new Store();
        $products = new Products();
        $cart = new Cart();
        

        if(!isset($_SESSION['cart']) || (isset($_SESSION['cart']) && count($_SESSION['cart']) == 0)) {
            header("Location: ".BASE_URL);
            exit;
        }

        $dados = $store->getTemplateData();

        $dados['list'] = $cart->getList();

        $this->loadTemplate('cart', $dados);
    }

    
    public function salvarInfo(){
       
        $p = new Products();
        $c = new Cart();
        // 08-04-19 -> voce ainda não tem a classe User, a linha abaixo foi marcada como comentario
        // o usuario será tratado como padrao 1
        //$u = new Users();
        //todas estas variaveis abaixo terão que ser criadas
        $_SESSION['user']='1';
        $_SESSION['id_pedido']='1234';
        $_SESSION['shipping']['price']='10';


        $cart = array();
        $today = date("Y-m-d H:i:s");
        $cart['item'] = $_SESSION['cart'];
        $dados = array();
        
        if(!empty($_SESSION['cart'])){
              foreach($cart['item'] as $item => $value){
                 $item = $p->getProduct($item);
                 $cart['items']['i'] = array(

                    "id_compra" =>  $_SESSION["id_pedido"][$_SESSION["user"]].time(),

                     "id_product" => $value,
                    "name" => $item['name'],
                    "id_user" => $_SESSION['user'],
                    "price" => $item['price'],
                    "valor_frete" => $_SESSION['shipping']['price'],
                    "qt" => $value,
                    "subtotal" => $item['price'] * $value,
                    "total" => floatval($item['price'] * $value) + $_SESSION['shipping']['price'],
                     "data" => $today,
                     "status" => "1",
                     "cep" => ""
  
                 );
        
           $c->salvarInfo($cart['items']);
              }
        }
        
        $dados['infos'] = $c->getPedidos($_SESSION['user']);
        $this->loadView("salvar-infos", $dados);
         }



    public function del($id) {
        if(!empty($id)) {
            unset($_SESSION['cart'][$id]);
        }

        header("Location: ".BASE_URL."cart");
        exit;
    }

    public function add() {

        if(!empty($_POST['id_product'])) {
            $id = intval($_POST['id_product']);
            $qt = intval($_POST['qt_product']);

            if(!isset($_SESSION['cart'])) {
                $_SESSION['cart'] = array();
            }

            if(isset($_SESSION['cart'][$id])) {
                $_SESSION['cart'][$id] += $qt;
            } else {
                $_SESSION['cart'][$id] = $qt;
            }
        }

        header("Location: ".BASE_URL."cart");
        exit;

    }

}