<aside>
							<h1>Filtro</h1>
							
				  		<div class="filterarea">
								<form method="GET">

								<input type="hidden" name="s" value="<?php echo (!empty($viewData['searchTerm']))?$viewData['searchTerm']:''; ?>" />

								<input type="hidden" name="category" value="<?php echo (!empty($viewData['category']))?$viewData['category']:''; ?>" />
									 	

										<div class="filterbox"> 
													<div class="filtertitle"> Marcas: </div>
									 						<?php foreach($viewData['filters']['brands'] as $bitem): ?>
															 <div class="filteritem">

																	<input type="checkbox"  <?php echo (isset($viewData['filters_selected']['brand']) && in_array($bitem['id'], $viewData['filters_selected']['brand']))?'checked="checked"':'' ; ?>
																	
																	value="<?php echo $bitem['id']; ?>" name="filter[brand][]" id="filter_brand<?php echo $bitem['id']; ?>" /> 

																	<label for="filter_brand<?php echo $bitem['id']; ?>"><?php echo $bitem['name']; ?>																																								
																	</label>
																	<div style="float:right">(<?php echo $bitem['count'] ; ?>) </div> 
													</div>
									<?php endforeach; ?>
													<div class="filtercontent"> 
													</div>
											</div>

											<div class="filterbox"> 
													<div class="filtertitle"> Preço: </div>
																						
													<div class="filtercontent"> 

													<input type="hidden" id="slider0" name="filter[slider0]" value="<?php echo $viewData['filters']['slider0']; ?>" />

													<input type="hidden" id="slider1" name="filter[slider1]" value="<?php echo $viewData['filters']['slider1']; ?>" />
													
													<p>
												<input type="text" id="amount" readonly style="border:0; color:#19070B; font-weight:bold;">
									 					</p>
											
													<div id="slider-range"></div>
													</div>
											</div>

											<div class="filterbox"> 
												<div class="filtertitle"> Nota dos consumidores: </div>
													<div class="filtercontent">
													<div class="filteritem">

													<input type="checkbox" <?php echo (isset($viewData['filters_selected']['star']) && in_array('0', $viewData['filters_selected']['star']))?'checked="checked"':'' ; ?> value="0" name="filter[star][]" id="filter_star0" /> 

																	<label for="filter_star0"> 
									 								Sem Avaliação
																	 
																		</label> 
																		<span style="float:right">(<?php echo $viewData['filters']['stars']['0'] ; ?>) </span> 

																		</div>
									 
									 				<div class="filteritem">
													 
																	<input type="checkbox" <?php echo (isset($viewData['filters_selected']['star']) && in_array('1', $viewData['filters_selected']['star']))?'checked="checked"':'' ; ?> 
																	
																	value="1" name="filter[star][]" id="filter_star1" /> 
																	<label for="filter_star1"> 
																	
																	<img src="<?php echo BASE_URL; ?>assets/images/starr.jpg" height="13px" border="0" /> 
																	

																	</label>
																	
																	<span style="float:right">(<?php echo $viewData['filters']['stars']['1'] ; ?>) </span>
													</div>
													<div class="filteritem">
																	<input type="checkbox" <?php echo (isset($viewData['filters_selected']['star']) && in_array('2', $viewData['filters_selected']['star']))?'checked="checked"':'' ; ?> 
																	
																	name="filter[star][]" id="filter_star2" value="2" /> 
																	<label for="filter_star2"> 
																	<img src="<?php echo BASE_URL; ?>assets/images/starr.jpg" height="13px" border="0" /> 
																	<img src="<?php echo BASE_URL; ?>assets/images/starr.jpg" height="13px" border="0" /> 
																	
																	</label>
																	<span style="float:right">(<?php echo $viewData['filters']['stars']['2'] ; ?>) </span>
													</div>

													<div class="filteritem">
																	<input type="checkbox" <?php echo (isset($viewData['filters_selected']['star']) && in_array('3', $viewData['filters_selected']['star']))?'checked="checked"':'' ; ?> 
																	
																	name="filter[star][]" id="filter_star3" value="3" /> 
																	<label for="filter_star3"> <img src="<?php echo BASE_URL; ?>assets/images/starr.jpg" height="13px" border="0" /> 
																	<img src="<?php echo BASE_URL; ?>assets/images/starr.jpg" height="13px" border="0" /> 
																	<img src="<?php echo BASE_URL; ?>assets/images/starr.jpg" height="13px" border="0" /> 
																	
																	</label>
																	<span style="float:right">(<?php echo $viewData['filters']['stars']['3'] ; ?>) </span>
													</div>

													<div class="filteritem">
																	<input type="checkbox" <?php echo (isset($viewData['filters_selected']['star']) && in_array('4', $viewData['filters_selected']['star']))?'checked="checked"':'' ; ?> 
																																		
																	name="filter[star][]" id="filter_star4" value="4" /> 
																	
																	<label for="filter_star4"> <img src="<?php echo BASE_URL; ?>assets/images/starr.jpg" height="13px" border="0" /> 
																	<img src="<?php echo BASE_URL; ?>assets/images/starr.jpg" height="13px" border="0" /> 
																	<img src="<?php echo BASE_URL; ?>assets/images/starr.jpg" height="13px" border="0" /> 
																	<img src="<?php echo BASE_URL; ?>assets/images/starr.jpg" height="13px" border="0" />
																	
																	
																	
																	</label>
																	<span style="float:right">(<?php echo $viewData['filters']['stars']['4'] ; ?>) </span>
													</div>

													<div class="filteritem">
																	<input type="checkbox" <?php echo (isset($viewData['filters_selected']['star']) && in_array('5', $viewData['filters_selected']['star']))?'checked="checked"':'' ; ?> 
																	
																	
																	name="filter[star][]" id="filter_star5" value="5" /> 
																	<label for="filter_star"> <img src="<?php echo BASE_URL; ?>assets/images/starr.jpg" height="13px" border="0" /> 
																	<img src="<?php echo BASE_URL; ?>assets/images/starr.jpg" height="13px" border="0" /> <img src="<?php echo BASE_URL; ?>assets/images/starr.jpg" height="13px" border="0" /> <img src="<?php echo BASE_URL; ?>assets/images/starr.jpg" height="13px" border="0" /> <img src="<?php echo BASE_URL; ?>assets/images/starr.jpg" height="13px" border="0" /> 
																	
																	</label>
																	<span style="float:right">(<?php echo $viewData['filters']['stars']['5'] ; ?>) </span>
													</div>

													</div>
											</div>

											<div class="filterbox"> 
													<div class="filtertitle"> Promoções: </div>
													
													<div class="filtercontent"> 
													<input type="checkbox" name="filter[sale]" <?php echo (isset($viewData['filters_selected']['sale']) && $viewData['filters_selected']['sale'] == '1' )?'checked="checked"':'' ; ?> 
													
													value="1" id="filter_sale" /> <label for="filter_sale"> Em Promoção </label>
													<span style="float:right">(<?php echo $viewData['filters']['sale'] ; ?>) </span> 
													</div>
											</div>

											<div class="filterbox"> 
													<div class="filtertitle"> Opções: </div> 
													<div class="filtercontent"> 
									 							<?php foreach($viewData['filters']['options'] as $option): ?>

																 <strong><?php echo $option['name']; ?> </strong> <br/>
									 									<?php foreach($option['options'] as $op): ?>

																		 <div class="filteritem">
																	<input type="checkbox"  
																	
																	<?php echo (isset($viewData['filters_selected']['options']) && in_array($op['value'], $viewData['filters_selected']['options']))?'checked="checked"':'' ; ?> 
																	
																	value="<?php echo $op['value']; ?>" name="filter[options][]" 
																	
																	id="filter_options<?php echo $op['id']; ?>" /> 

																	<label for="filter_options <?php echo $op['id']; ?>">
																	
																	<?php echo $op['value']; ?>																																								
																	</label>
																	<div style="float:right">(<?php echo $op['count'] ; ?>) </div>

																		 <?php endforeach; ?>
																 <br/>

															<?php endforeach; ?>
																 
													</div>
											</div>
														</form>
								 	</div>						
					  	
				  	</aside>